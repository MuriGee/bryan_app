//
//  CategoryItem.swift
//  BryanApp
//
//  Created by Muri Gumbodete on 26/08/2020.
//  Copyright © 2020 MuriGee. All rights reserved.
//

import Foundation

struct CategoryItem {
    // Array for categories of products.
    static let category = ["Aggregate", "Bricks", "Cement", "Concrete", "Glass", "Lime", "Metals", "Mortars", "Paints", "Plastics", "Steel", "Stones", "Tar Bitumen", "Tiles", "Timbers"]
}
