//
//  ProductItem.swift
//  BryanApp
//
//  Created by Muri Gumbodete on 23/07/2020.
//  Copyright © 2020 MuriGee. All rights reserved.
//

import Foundation
import Firebase

protocol DocumentSerializeable {
    init?(dictionary:[String:Any])
}

struct ProductItem {
    let title: String
    let category: String
    let price: Double
    let amount: String
    let location: String
    let imageUrl: String
    let description: String
    let seller: String
    let docID: String
    let uid: String
    
    var dictionary: [String:Any] {
        return ["title": title,
                "category": category,
                "price": price,
                "amount": amount,
                "location": location,
                "imageUrl": imageUrl,
                "description": description,
                "seller": seller,
                "docID" : docID,
                "uid": uid
        ]
    }
}

extension ProductItem : DocumentSerializeable {
    init?(dictionary: [String : Any]) {
        guard let title = dictionary["title"] as? String,
              let category = dictionary["category"] as? String,
            let price = dictionary["price"] as? Double,
            let amount = dictionary["amount"] as? String,
            let location = dictionary["location"] as? String,
            let imageUrl = dictionary["imageUrl"] as? String,
            let description = dictionary["description"] as? String,
            let seller = dictionary["seller"] as? String,
            let docID = dictionary["docID"] as? String,
            let uid = dictionary["uid"] as? String
            else { return nil }
        
        self.init(title: title, category: category, price: price, amount: amount, location: location, imageUrl: imageUrl, description: description, seller: seller, docID: docID, uid: uid)
    }
}
