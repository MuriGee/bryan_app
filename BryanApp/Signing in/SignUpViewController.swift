//
//  SignUpViewController.swift
//  BryanApp
//
//  Created by Muri Gumbodete on 25/05/2020.
//  Copyright © 2020 MuriGee. All rights reserved.
//

import UIKit
import Firebase

class SignUpViewController: UIViewController {

    @IBOutlet weak var nameTextField: UITextField!
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var secPasswordTextField: UITextField!
    @IBOutlet weak var signUpButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let tap = UITapGestureRecognizer(target: self.view, action: #selector(UIView.endEditing(_:)))
        view.addGestureRecognizer(tap)
        // Do any additional setup after loading the view.
    }
    
    @IBAction func signUpButtonTouched(_ sender: Any) {
        guard
            let password = passwordTextField.text,
            let secPassword = secPasswordTextField.text,
            password == secPassword
        else {
            return
        }
        guard
            let name = nameTextField.text,
            let email = emailTextField.text,
            name.count > 0,
            email.count > 0
        else {
            return
        }
        
        Auth.auth().createUser(withEmail: emailTextField.text!, password: passwordTextField.text!) { user, error in
            if error == nil {
                print("Success")
                self.clearTextfields()
            } else {
                print(error?.localizedDescription as Any)
            }
        }
    }
    
    func clearTextfields() {
        //clears the text fields
        nameTextField.text = ""
        emailTextField.text = ""
        passwordTextField.text = ""
        secPasswordTextField.text = ""
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
}
