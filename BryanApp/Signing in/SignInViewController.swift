//
//  ViewController.swift
//  BryanApp
//
//  Created by Muri Gumbodete on 16/05/2020.
//  Copyright © 2020 MuriGee. All rights reserved.
//

import UIKit
import Firebase

class SignInViewController: UIViewController {
    
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var signInButton: UIButton!
    @IBOutlet weak var segmentedControl: UISegmentedControl!
    
    var db: Firestore!
    let radius = 5
    
    override func viewDidLoad() {
        super.viewDidLoad()
        authListener()
        setUpVariables()
        configureView()
        // Do any additional setup after loading the view.
    }
    
    //if user profile found, function checks database to find if the logged in user is a buyer or seller and then sends to appropriate view.
    func authListener() {
        Auth.auth().addStateDidChangeListener() { auth, user in
            if user != nil {
                self.db.collection("buyers").document(Auth.auth().currentUser!.uid).getDocument { (document, error) in
                    if let document = document, document.exists {
                        self.performSegue(withIdentifier: "buyerLogin", sender: self)
                    }
                }
                self.db.collection("sellers").document(Auth.auth().currentUser!.uid).getDocument { (document, error) in
                    if let document = document, document.exists {
                        self.performSegue(withIdentifier: "sellerLogin", sender: self)
                    }
                }
            }
        }
    }
    
    func setUpVariables() {
        db = Firestore.firestore()
        emailTextField.delegate = self
        passwordTextField.delegate = self
        let tap = UITapGestureRecognizer(target: self.view, action: #selector(UIView.endEditing(_:)))
        view.addGestureRecognizer(tap)
    }
    
    func configureView() {
        emailTextField.layer.borderWidth = 1
        emailTextField.layer.masksToBounds = true
        emailTextField.layer.cornerRadius = CGFloat(radius)
        passwordTextField.layer.borderWidth = 1
        passwordTextField.layer.masksToBounds = true
        passwordTextField.layer.cornerRadius = CGFloat(radius)
        signInButton.layer.borderWidth = 1
        signInButton.layer.cornerRadius = 13
    }
    
    //Authenticates sign in details to database and approves or denies request.
    func signIn() {
        Auth.auth().signIn(withEmail: emailTextField.text!, password: passwordTextField.text!) { user, error in
            if let error = error, user == nil {
                let alert = UIAlertController(title: "Sign In Failed",
                                              message: error.localizedDescription,
                                              preferredStyle: .alert)
                
                alert.addAction(UIAlertAction(title: "OK", style: .default))
                
                self.present(alert, animated: true, completion: nil)
            } else { self.clearTextfields() }
        }
    }
    
    //Clears the text fields
    func clearTextfields() {
        emailTextField.text = ""
        passwordTextField.text = ""
    }

    @IBAction func signInButtonTouched(_ sender: Any) {
        //checks which segmented is selected (Buyer or Seller) then checks if user is found in respective database and if conditions are met successfully logs in, if not pops up to tell you error.
        switch segmentedControl.selectedSegmentIndex {
        case 0:
            db.collection("buyers").whereField("emailAddress", isEqualTo: emailTextField.text!).getDocuments() { (querySnapshot, err) in
                if querySnapshot!.isEmpty {
                    let alert = UIAlertController(title: "Email Address Not found", message: "Unfortunately no account with this email address has been found. Are you looking to buy? if yes try creating a account or switch the to the seller login thats not highlighted under the title.", preferredStyle: .alert)
                    alert.addAction(UIAlertAction(title: "Back", style: .default, handler: { action in self.navigationController?.popViewController(animated: true)
                    }))
                    self.present(alert, animated: true)
                } else {
                    self.signIn()
                }
            }
        case 1:
            db.collection("sellers").whereField("emailAddress", isEqualTo: emailTextField.text!).getDocuments() { (querySnapshot, err) in
                if querySnapshot!.isEmpty {
                    let alert = UIAlertController(title: "Email Address Not found", message: "Unfortunately no account with this email address has been found. Are you looking to Sell? if yes try creating a account or switch the to the buyer login thats not highlighted under the title.", preferredStyle: .alert)
                    alert.addAction(UIAlertAction(title: "Back", style: .default, handler: { action in self.navigationController?.popViewController(animated: true)
                    }))
                    self.present(alert, animated: true)
                } else {
                    self.signIn()
                }
            }
        default:
            break
        }
    }
    
    @IBAction func segmentedControlTapped(_ sender: Any) {
    }
    
}

extension SignInViewController: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        let nextTag = textField.tag + 1

        if let nextResponder = textField.superview?.viewWithTag(nextTag) {
            nextResponder.becomeFirstResponder()
        } else {
            signIn()
        }
        
        return true
    }
}
