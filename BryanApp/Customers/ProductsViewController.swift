//
//  ProductsViewController.swift
//  BryanApp
//
//  Created by Muri Gumbodete on 23/07/2020.
//  Copyright © 2020 MuriGee. All rights reserved.
//

import UIKit
import Firebase

class ProductsViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    var db: Firestore!
    var ProductArray = [ProductItem]()
    var categoryID: String!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setUpVariables()
        loadData()
    }
    
    // Initialises variables.
    func setUpVariables() {
        title = "Products"
        navigationItem.largeTitleDisplayMode = .never
        tableView.dataSource = self
        tableView.delegate = self
        db = Firestore.firestore()
    }
    
    // Filters the products depending on category chosen and places into Array defined in ProductItem struct and displays data in tableView.
    func loadData() {
        db.collection("products").whereField("category", isEqualTo: categoryID!).getDocuments() { (querySnapshot, err) in
            if querySnapshot!.isEmpty {
                let alert = UIAlertController(title: "Uh Oh Sorry", message: "Unfortunately no results have been found for your chosen category. Come back later and see if any vendors have uploaded products.", preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: "Back", style: .default, handler: { action in self.navigationController?.popViewController(animated: true)
                }))
                self.present(alert, animated: true)
            } else {
                self.ProductArray = querySnapshot!.documents.compactMap({ProductItem(dictionary: $0.data())})
                self.tableView.reloadData()
            }
        }
    }
}

// MARK: - Navigation
extension ProductsViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        performSegue(withIdentifier: "displayProduct", sender: self)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let indexPath = tableView.indexPathsForSelectedRows?.first {
            let destinationVC = segue.destination as! CustomerProductDisplayViewController
            let productID = ProductArray[indexPath.row]
            destinationVC.productID = productID
        }
    }
}

extension ProductsViewController: UITableViewDataSource {
    // Counts number of items in Array from Firebase and creates tableview rows to place items.
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return ProductArray.count
    }
    
    // Using defined struct, adds data from array and displays it in appropriate field in the tableView.
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! ProductTableViewCell
        let product = ProductArray[indexPath.row]
        cell.sellerName?.text = product.seller
        cell.productTitle?.text = product.title
        let price = product.price
        cell.productPrice?.text = String(format: "$%.2f", price)
        cell.productLocation?.text = product.location
        //Example image. To be changed to read sellers image descrition.
        cell.productImage?.image = UIImage(named: "aggregate")
        
        return cell
    }
}
