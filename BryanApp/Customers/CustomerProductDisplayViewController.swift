//
//  ProductDisplayViewController.swift
//  BryanApp
//
//  Created by Muri Gumbodete on 22/08/2020.
//  Copyright © 2020 MuriGee. All rights reserved.
//

import UIKit
import Firebase

class CustomerProductDisplayViewController: UIViewController {
    
    @IBOutlet weak var contentView: ReusableProductDisplay!
    
    var productID: ProductItem?

    override func viewDidLoad() {
        super.viewDidLoad()
        editButtonVisibility()
        title = productID?.title
        loadData()
    }
    
    func editButtonVisibility() {
        if productID?.uid == Auth.auth().currentUser?.uid {
        } else { navigationItem.rightBarButtonItem = nil }
    }
    
    func loadData() {
        contentView.titleLabel.text = productID?.title
        let price = productID?.price
        contentView.priceLabel.text = String(format: "$%.2f", price!)
        if (productID?.amount.count)! > 0 {
            contentView.amountLabel.text = "PER \((productID?.amount)!)"
        } else { contentView.amountLabel.text = "" }
        contentView.sellerName.text = "Sold by: " + productID!.seller
        contentView.locationLabel.text = productID?.location
        contentView.descriptionTextView.text = productID?.description
    }
    
    @IBAction func editButtonTapped(_ sender: Any) {
        performSegue(withIdentifier: "editProduct", sender: self)
    }
    
    // MARK: - Navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let destinationVC = segue.destination as! AddProductViewController
        destinationVC.productID = productID
        destinationVC.editBool = true
    }
}
