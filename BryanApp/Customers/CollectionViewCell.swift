//
//  CollectionViewCell.swift
//  BryanApp
//
//  Created by Muri Gumbodete on 20/07/2020.
//  Copyright © 2020 MuriGee. All rights reserved.
//

import UIKit

class CollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var categoryImage: UIImageView!
    @IBOutlet weak var categoryTitle: UILabel!
    
}
