//
//  HomeViewController.swift
//  BryanApp
//
//  Created by Muri Gumbodete on 19/05/2020.
//  Copyright © 2020 MuriGee. All rights reserved.
//

import UIKit
import Firebase

class HomeViewController: UIViewController {
    
    //MARK: - IBOutlets and Variables
    @IBOutlet var collectionView: UICollectionView!
    
    // Array for References for images in assets folder.
    let categoryImages: [UIImage] = [
        UIImage(named: "aggregate")!,
        UIImage(named: "bricks")!,
        UIImage(named: "cement")!,
        UIImage(named: "concrete")!,
        UIImage(named: "glass")!,
        UIImage(named: "lime")!,
        UIImage(named: "metals")!,
        UIImage(named: "mortars")!,
        UIImage(named: "paints")!,
        UIImage(named: "plastics")!,
        UIImage(named: "steel")!,
        UIImage(named: "stones")!,
        UIImage(named: "tar")!,
        UIImage(named: "tiles")!,
        UIImage(named: "timbers")!
    ]
    
    // MARK: - UIViewControllerEvents
    override func viewDidLoad() {
        super.viewDidLoad()
        title = "Categories"
        navigationController?.navigationBar.prefersLargeTitles = true
        collectionView.dataSource = self
        collectionView.delegate = self
        // Do any additional setup after loading the view.
    }
    
    // sends sign out request to Firebase.
    @IBAction func signOutButtonTouched(_ sender: Any) {
        try! Auth.auth().signOut()
        self.dismiss(animated: true, completion: nil)
    }
}

// MARK: - CollectionView Data Source
extension HomeViewController: UICollectionViewDataSource {
    // Creates collection View cells based on items in category array (15).
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return CategoryItem.category.count
    }
    
    // Sets the cell to get image and title data from cat arrays and places color and border.
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! CollectionViewCell
        
        cell.categoryTitle.text = CategoryItem.category[indexPath.item]
        cell.categoryImage.image = categoryImages[indexPath.item]
        cell.layer.borderColor = UIColor.lightGray.cgColor
        cell.layer.borderWidth = 0.5
        cell.layer.cornerRadius = 5
        
        return cell
    }
}

// MARK: - Navigation CollectionView Delegate
extension HomeViewController: UICollectionViewDelegate {
    
    // To be used to segue to products list for category tapped category.
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        performSegue(withIdentifier: "productPage", sender: self)
    }
    
    // Get the new view controller using segue.destination.
    // Pass the selected object to the new view controller.
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let indexPath = collectionView.indexPathsForSelectedItems?.first {
            let destinationVC = segue.destination as! ProductsViewController
            let categoryID = "\(CategoryItem.category[indexPath.item])"
            destinationVC.categoryID = categoryID
        }
    }
    
    // Used to highlight tapped cell grey.
    func collectionView(_ collectionView: UICollectionView, didHighlightItemAt indexPath: IndexPath) {
        if let cell = collectionView.cellForItem(at: indexPath) {
            cell.contentView.backgroundColor = #colorLiteral(red: 0.8039215803, green: 0.8039215803, blue: 0.8039215803, alpha: 1)
        }
    }

    // Used to unhighlight cell when user is no longer tapping on cell.
    func collectionView(_ collectionView: UICollectionView, didUnhighlightItemAt indexPath: IndexPath) {
        if let cell = collectionView.cellForItem(at: indexPath) {
            cell.contentView.backgroundColor = nil
        }
    }
}

extension HomeViewController: UICollectionViewDelegateFlowLayout {
    // Creates size of each cell displayed.
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: 160, height: 150)
    }
}

