//
//  SellerProductsTableViewController.swift
//  BryanApp
//
//  Created by Muri Gumbodete on 27/09/2020.
//  Copyright © 2020 MuriGee. All rights reserved.
//

import UIKit
import Firebase

class SellerProductsTableViewController: UITableViewController {

    // MARK: - Variables
    
    var db = Firestore.firestore()
    var ProductArray = [ProductItem]()
    var categoryID: String!
    
    // MARK: - UIViewControllerEvents
    override func viewDidLoad() {
        super.viewDidLoad()
        loadData()
    }
    
    // MARK: - Core Functions
    
    //Checks firebase to see if user has any products and returns into a Array if found. Sets alert to add products if no results returned.
    func loadData() {
        db.collection("products").whereField("uid", isEqualTo: Auth.auth().currentUser?.uid as Any).addSnapshotListener { querySnapshot, error in
            if querySnapshot!.isEmpty {
                let alert = UIAlertController(title: "No Products found.", message: "Would you like to add products", preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: "No", style: .cancel, handler: nil))
                alert.addAction(UIAlertAction(title: "Yes", style: .default, handler: { action in self.performSegue(withIdentifier: "addProduct", sender: self)
                }))
                self.present(alert, animated: true)
            } else {
                self.ProductArray = querySnapshot!.documents.compactMap({ProductItem(dictionary: $0.data())})
                self.tableView.reloadData()
            }
        }
    }
    
    // MARK: - Action Functions
    
    //Button so user can add prooducts.
    @IBAction func rightAddBarButtonTapped(_ sender: Any) {
        performSegue(withIdentifier: "addProduct", sender: self)
    }
    
    //Desitination View for unwinding segue
    @IBAction func unwindWithSegue(_ segue: UIStoryboardSegue) {
    }

    // MARK: - Table view data source

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // Return the number of rows from ProductArray
        return ProductArray.count
    }

    // populates cells with struct data recieved from Firebase.
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! ProductTableViewCell
        let product = ProductArray[indexPath.row]
        cell.sellerName?.text = product.seller
        cell.productTitle?.text = product.title
        let price = product.price
        cell.productPrice?.text = String(format: "$%.2f", price)
        cell.productLocation?.text = product.location
        //Example image. To be changed to read sellers "image descrition.
        cell.productImage?.image = UIImage(named: "aggregate")

        return cell
    }
    
    // MARK: - Navigation. Table View Delegate
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        performSegue(withIdentifier: "displayProduct", sender: self)
        print(ProductArray[indexPath.row] as Any)
    }
    
    // Creates segue parameteres, and places the selected cell into the struct parameters to be displayed in the Display View.
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let indexPath = tableView.indexPathsForSelectedRows?.first {
            let destinationVC = segue.destination as! CustomerProductDisplayViewController
            let productID = ProductArray[indexPath.row]
            destinationVC.productID = productID
        }
    }
}
