//
//  SellerViewController.swift
//  BryanApp
//
//  Created by Muri Gumbodete on 25/08/2020.
//  Copyright © 2020 MuriGee. All rights reserved.
//

import UIKit
import Firebase

class AddProductViewController: UIViewController {
    
    // MARK: - IBOutlets and Variables
    @IBOutlet weak var categoryTextField: UITextField!
    @IBOutlet weak var titleTextField: UITextField!
    @IBOutlet weak var priceTextField: UITextField!
    @IBOutlet weak var amountTextField: UITextField!
    @IBOutlet weak var locationTextField: UITextField!
    @IBOutlet weak var previewButton: UIButton!
    @IBOutlet weak var descriptionTextView: UITextView!
    
    let radius = 5
    let pickerView = UIPickerView()
    var db: Firestore!
    let placeholder = "Please describe your product."
    var editBool = false
    var productID: ProductItem?
    
    // MARK: - UIViewControllerEvents
    override func viewDidLoad() {
        super.viewDidLoad()
        configureView()
        setUpVariables()
        // Do any additional setup after loading the view.
    }
    
    // MARK:- Set Up Functions
    func configureView() {
        categoryTextField.layer.borderWidth = 1
        categoryTextField.layer.masksToBounds = true
        categoryTextField.layer.cornerRadius = CGFloat(radius)
        titleTextField.layer.borderWidth = 1
        titleTextField.layer.masksToBounds = true
        titleTextField.layer.cornerRadius = CGFloat(radius)
        priceTextField.layer.borderWidth = 1
        priceTextField.layer.masksToBounds = true
        priceTextField.layer.cornerRadius = CGFloat(radius)
        amountTextField.layer.borderWidth = 1
        amountTextField.layer.masksToBounds = true
        amountTextField.layer.cornerRadius = CGFloat(radius)
        locationTextField.layer.borderWidth = 1
        locationTextField.layer.masksToBounds = true
        locationTextField.layer.cornerRadius = CGFloat(radius)
        previewButton.layer.borderWidth = 1
        previewButton.layer.cornerRadius = 13
        descriptionTextView.layer.borderWidth = 1
        descriptionTextView.layer.masksToBounds = true
        descriptionTextView.layer.cornerRadius = CGFloat(radius)
        descriptionTextView.text = placeholder
        descriptionTextView.textColor = UIColor.lightGray
        if editBool == true {
            enterProductData()
        }
    }
    
    func enterProductData() {
        categoryTextField.text = productID?.category
        titleTextField.text = productID?.title
        priceTextField.text = String(productID!.price)
        amountTextField.text = productID?.amount
        locationTextField.text = productID?.location
        descriptionTextView.text = productID?.description
    }
    
    func setUpVariables() {
        db = Firestore.firestore()
        pickerView.dataSource = self
        pickerView.delegate = self
        descriptionTextView.delegate = self
        categoryTextField.inputView = pickerView
        let tap = UITapGestureRecognizer(target: self.view, action: #selector(UIView.endEditing(_:)))
        view.addGestureRecognizer(tap)
    }
    
    // MARK: - Core Functions
    // Checks textfields to make sure data there is data inputted and the data is of the right value type before sending off to segue.
    func checkData() {
        guard
            let category = categoryTextField.text,
            let title = titleTextField.text,
            let price = priceTextField.text,
            let location = locationTextField.text,
            category.count > 0,
            title.count > 0,
            price.count > 0,
            location.count > 0
        else {
            let alert = UIAlertController(title: "Something's wrong", message: "Please check all fields are completed properly and try again.", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "Back", style: .default, handler: nil))
            return self.present(alert, animated: true)
        }
        performSegue(withIdentifier: "previewProduct", sender: self)
    }
    
    // MARK: - Navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let destinationVC = segue.destination as! SellerProductDisplayViewController
        if editBool == true {
            destinationVC.docID = productID!.docID
        }
        destinationVC.categoryString = categoryTextField.text!
        destinationVC.titleString = titleTextField.text!
        destinationVC.priceValue = Double(priceTextField.text!)!
        destinationVC.amountValue = amountTextField.text!
        if descriptionTextView.text.contains(placeholder) {
            destinationVC.descriptionString = ""
        } else { destinationVC.descriptionString = descriptionTextView.text! }
        destinationVC.locationString = locationTextField.text!
    }
    
    //Initialises checks of text fields.
    @IBAction func previewButtonTapped(_ sender: Any) {
        checkData()
    }
    
    //Signs user out of account and returns to homepage.
    @IBAction func signOutButtonTouched(_ sender: Any) {
        try! Auth.auth().signOut()
        self.dismiss(animated: true, completion: nil)
    }
}

// MARK: - Picker View Delegate
extension AddProductViewController: UIPickerViewDelegate {
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        categoryTextField.text = CategoryItem.category[row]
        // FIXME: - Doesn't allow picking first value unless you scroll.
        //self.view.endEditing(true)
    }
}

// MARK: - Picker View Data Source
extension AddProductViewController: UIPickerViewDataSource {
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return CategoryItem.category.count
    }
    
    func pickerView( _ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return CategoryItem.category[row]
    }
}

extension AddProductViewController: UITextViewDelegate {
    func textViewDidBeginEditing(_ textView: UITextView) {
        if descriptionTextView.textColor == UIColor.lightGray {
            descriptionTextView.text = nil
            descriptionTextView.textColor = UIColor.black
        }
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        if descriptionTextView.text.isEmpty {
            descriptionTextView.text = placeholder
            descriptionTextView.textColor = UIColor.lightGray
        }
    }
}
