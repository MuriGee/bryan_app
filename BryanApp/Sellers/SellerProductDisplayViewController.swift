//
//  SellerProductDisplayViewController.swift
//  BryanApp
//
//  Created by Muri Gumbodete on 25/09/2020.
//  Copyright © 2020 MuriGee. All rights reserved.
//

import UIKit
import Firebase

class SellerProductDisplayViewController: UIViewController {
    
    @IBOutlet weak var contentView: ReusableProductDisplay!
    @IBOutlet weak var postButton: UIButton!
    
    var docID = String()
    var categoryString = String()
    var titleString = String()
    var priceValue = Double()
    var amountValue = String()
    var sellerString = String()
    var locationString = String()
    var descriptionString = String()
    var db = Firestore.firestore()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configureView()
        loadData()
        // Do any additional setup after loading the view.
    }
    
    func configureView() {
        postButton.layer.borderWidth = 1
        postButton.layer.cornerRadius = 13
    }
    
    func loadData() {
        db.collection("sellers").document(Auth.auth().currentUser!.uid).getDocument { (document, error) in
        if let document = document, document.exists {
            self.sellerString = document.get("Name") as! String
            self.contentView.sellerName.text = "Sold by: \(self.sellerString)"
            }
        }
        
        contentView.titleLabel.text = titleString
        contentView.priceLabel.text = String(format: "$%.2f", priceValue)
        contentView.descriptionTextView.text = descriptionString
        contentView.locationLabel.text = locationString
        if amountValue.count < 1 {
            contentView.amountLabel.text = ""
        } else { contentView.amountLabel.text = "PER \(amountValue)" }
    }
    
    //Writes product details to Firestore Data and if successful returns to My Products Page
    func writeData() {
        if docID.isEmpty {
            docID = UUID().uuidString
        }
        db.collection("products").document(docID).setData([
            "category" : categoryString,
            "title" : titleString,
            "price" : priceValue,
            "amount" : amountValue,
            "location" : locationString,
            "imageUrl" : "",
            "seller" : sellerString,
            "description" : descriptionString,
            "docID" : docID,
            "uid" : Auth.auth().currentUser?.uid as Any
        ]) { err in
            if let err = err {
                print("Error writing document: \(err)")
            } else {
                print("Document successfully written!")
                self.performSegue(withIdentifier: "unwindToProducts", sender: self)
            }
        }
    }
    
    // MARK: - Navigation
    @IBAction func postButtonTapped(_ sender: Any) {
        writeData()
    }
}
